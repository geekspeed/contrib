#!/usr/bin/perl -w
use Encode;
use VMware::VIRuntime;
use VMware::VILib;
use Getopt::Long;
use IO::File;
use POSIX;

my $startTime;
my $stopTime;
my $server;
my $sleepTime = 60;
my $userName;
my $passWord;
my $timeStamp;
my $timeStampFile = "timestamp.txt";
my $timeNow;

my $optRs = GetOptions('s=s'=>\$server, 'u=s'=>\$userName, 'p=s'=>\$passWord, 't=s'=>\$sleepTime, 'r=s'=>\$timeStampFile);
Getopt::Long::Configure("pass_through");

if((not defined $server) or (not defined $userName) or (not defined $passWord)){
        print "Usage: -s=server.ip.addr -u=username -p=password -t=pollinterval(seconds, optional, default=$sleepTime) -r=timestampfile (optional, default=$timeStampFile, format:%Y-%m-%dT%H:%M:%S)";
        exit;
}

my $file = new IO::File("< $timeStampFile");
if (defined $file) {
       my ($line);
       while (<$file>) {
              $line = $_;
              if ($line =~ /^#/) {next;} else {last;}
       }
       if (not ($line =~ /^#/)) {
              $startTime=$line;
              $startTime=~s/[\n]+//g;
       }
       $file->close();
}

$ENV{'VI_SERVER'}=$server;
$ENV{'VI_USERNAME'}=$userName;
$ENV{'VI_PASSWORD'}=$passWord;

my %opts = (
   vmname  => {
      type     => "=s",
      variable => "name",
      help     => "Name of the Virtual Machine",
      required => 0
   },
);

Opts::add_options(%opts);
Opts::parse();
Opts::validate();

while (1) {
       $timeNow = substr(POSIX::strftime('%Y-%m-%dT%H:%M:%S',localtime(time)),0,19);

       if(not defined $startTime) {
              $startTime = substr(POSIX::strftime('%Y-%m-%dT%H:%M:%S',localtime(time-$sleepTime)),0,19);
       }
       $stopTime = $timeNow;

       eval{
              Util::connect()
       };
       if($@){
              print "{\"EventTime\":\"$timeNow\",\"Message\":\"$@\",\"UserName\":\"$userName\"}\n";
       }
       eval{
              getLog()
       };
       if($@){
              print "{\"EventTime\":\"$timeNow\",\"Message\":\"$@\",\"UserName\":\"$userName\"}\n";
       }
       eval{
              Util::disconnect()
       };
       sleep($sleepTime);
}

sub getLog {
  my $eventNum=0;
  my ($eventArray,$lSize);
  my $getFunc = Vim::get_service_content()->eventManager;
  my $eventManager = Vim::get_view(mo_ref => $getFunc);
        my $eventFilter = EventFilterSpecByTime->new(beginTime=>$startTime,endTime=>$stopTime);
  my $filterSpec = EventFilterSpec->new(time=>$eventFilter);
  my $collectorViewRef= $eventManager->CreateCollectorForEvents(filter=>$filterSpec);
  my $collectorView = Vim::get_view(mo_ref => $collectorViewRef);
  my $separator="\":\"";
  my $separator2="\",\"";

  while (1) {
    $eventArray=$collectorView->ReadNextEvents(maxCount=>400);
    my @evtArray = @{$eventArray};
    if (not defined $eventArray) { last;}

    $lSize = @evtArray;
    if ($lSize>=1) {
      foreach (@evtArray){
        my $fullMsg = decode_utf8($_->fullFormattedMessage);
        $fullMsg=~s/[^[:ascii:]]+//g;
        $fullMsg=~s/[\n]+//g;
        my $packetContent="{";
        if (defined($_->createdTime)) {

          $packetContent=$packetContent."\"EventTime".$separator.$_->createdTime.$separator2.
            "Message".$separator.$fullMsg.$separator2.
            "UserName".$separator.$_->userName."\"";
          if ($startTime eq $_->createdTime) {next;}
          $timeStamp=$_->createdTime;
        }
        if (defined($_->vm)) {
          my $vmObj = $_->vm;
          $packetContent=$packetContent.",\""."VirtualMachine".$separator.$vmObj->name."\"";
        }
        if (defined($_->host)) {
          my $hostObj=$_->host;
          $packetContent=$packetContent.",\""."HostName".$separator.$hostObj->name."\"";
        }
        $packetContent.="}\n";
                                print $packetContent;
        $eventNum++;
      }
    }
                else {
                        if (defined $timeStamp) {
                                $startTime = $timeStamp;
                          my $file = new IO::File("+> $timeStampFile");
                    $file->print($timeStamp);
                      $file->close();
                        }
      last;
    }
  }
}
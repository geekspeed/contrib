import Evtx.Evtx as evtx
import Evtx.Views as e_views

import nxlog

def read_data(module):
    nxlog.log_debug('Starting Processing EVTX')
    with evtx.Evtx('/tmp/security.evtx') as log:
        for record in log.records():
            event = module.logdata_new()
            event.set_field('Message', record.xml())
            event.post()
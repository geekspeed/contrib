#!/usr/bin/env bash

my_package_dir="${MY_SCRIPT_PATH}/${MY_NAME}-${MY_VERSION}"
tmp_addon_dir_path="${my_package_dir}/${NXLOG_ADDON_PATH}"

if [ -d ${my_package_dir} ]; then
  rm -rf ${my_package_dir}
fi

mkdir -p "${my_package_dir}/DEBIAN"

echo """Package: ${MY_NAME}
Version: ${MY_VERSION}
Maintainer: Botond Botyanszki <boti@nxlog.org>
Architecture: all
Depends: nxlog | nxlog-trial, python-enum34
Description: Admin Tools addon for NXLog
 NXLog is a modular, multi-threaded, high-performance log management solution.
 .
 This package contains the Admin Tools addon for NXLog.
""" > ${my_package_dir}/DEBIAN/control

# create postinst file
echo """#!/bin/sh

set -e

BASE=${NXLOG_ADDON_PATH}

case \"\$1\" in
  configure)

    if [ ! -d /opt/nxlog/var/log/nxlog ]; then
      echo \"ERROR: NXLog is not installed, or installation is broken\"
      exit 1
    fi

    MY_USER=\`stat -c \"%U\" /opt/nxlog/var/log/nxlog\`
    MY_GROUP=\`stat -c \"%G\" /opt/nxlog/var/log/nxlog\`
    chown \${MY_USER}:\${MY_GROUP} \${BASE}/var
    ;;

  abort-upgrade|abort-remove|abort-deconfigure)
    ;;

  *)
    echo \"postinst called with unknown argument \$1\" >&2
    exit 1
    ;;
esac

exit 0
""" > ${my_package_dir}/DEBIAN/postinst
chmod 0755 ${my_package_dir}/DEBIAN/postinst

# create postrm file
echo """#!/bin/sh

set -e

BASE=${NXLOG_ADDON_PATH}

case \"\$1\" in
  purge)
    [ -d \${BASE} ] && rm -rf \${BASE}
    ;;

  remove)
    [ -d \${BASE} ] && chown -R 0:0 \${BASE} >/dev/null 2>&1
    [ -d \${BASE} ] && find \${BASE} -type f -a \( -name '*.log' -or -name '*.pyc' \) -delete >/dev/null 2>&1
    [ -d \${BASE} ] && find \${BASE} -type d -empty -delete >/dev/null 2>&1
    ;;

  upgrade|failed-upgrade|abort-install|abort-upgrade|disappear)
    ;;

  *)
    echo \"postrm called with unknown argument \$1\" >&2a
    exit 1
    ;;
esac

exit 0
""" > ${my_package_dir}/DEBIAN/postrm
chmod 0755 ${my_package_dir}/DEBIAN/postrm

# copy documentation
mkdir -p "${tmp_addon_dir_path}/doc"
cp "${MY_SCRIPT_PATH}/../"{license,copyright}.txt "${tmp_addon_dir_path}/doc"
#cp "${MY_SCRIPT_PATH}/static_files/README_lin.txt" "${tmp_addon_dir_path}/doc/README.txt"
#if [ -d "${MY_SCRIPT_PATH}/../doc/output" ]; then
#  cp -r ${MY_SCRIPT_PATH}/../doc/output/* "${tmp_addon_dir_path}/doc/"
#else
#  echo "WARNING: no documentation found"
#  echo "MARK: UNSTABLE"
#fi

cp -r "${MY_SCRIPT_PATH}"/../src/* "${tmp_addon_dir_path}"
# todo: To copy the keys, remove next line if necessary
rm -f "${tmp_addon_dir_path}"/keys/*
chmod 0700 "${tmp_addon_dir_path}"/keys

rm -f "${tmp_addon_dir_path}"/var/*

echo "${NXLOG_ADDON_PATH}/nxlog-admin.conf.json" > "${my_package_dir}/DEBIAN/conffiles"

# workaround for setting the owner on the files
sudo chown -R 0:0 "${my_package_dir}"
sudo find "${my_package_dir}" -type d -exec chmod 0755 \{\} \;
sudo find "${my_package_dir}" -type f -exec chmod 0644 \{\} \;
sudo chmod +x "${my_package_dir}/DEBIAN/"{control,postinst,postrm}

# create package
dpkg-deb --build ${my_package_dir} || { echo "ERROR: dpkg-deb failed, exiting"; exit 1; }

cp ${my_package_dir}/DEBIAN/{control,postinst,postrm} ../../
mv *deb ../..

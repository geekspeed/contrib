from api.action import Action
import asyncore


class ActionExit(Action):

    def __init__(self):
        Action.__init__(self, "exit", 'stop nxlog manager')

    def execute(self, core, session):
        core.need_exit = True


class ActionList(Action):

    def __init__(self):
        Action.__init__(self, 'list', 'get all commands list')

    def execute(self, core, session):
        names = [name for name in self.actions]
        names.sort()
        for name in names:
            action = self.actions[name]
            session.add_element(
                tag='actions',
                name=action.name,
                description=action.description
            )


class ActionTimer(Action):

    def __init__(self):
        Action.__init__(self, "timer", 'run timer ')

    def execute(self, core, session):
        core.call("agent.check")
        session.add_params(result='ok')

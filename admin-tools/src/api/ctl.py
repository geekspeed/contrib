"""
IO routines for client control
"""

import asyncore
import socket
import logging
import api.action
import os


class Connection(asyncore.dispatcher):
    """
    dispatcher for control socket
    """

    def __init__(self, ctlSocketConf, core):
        asyncore.dispatcher.__init__(self)

        self.conf = ctlSocketConf
        self.file = ctlSocketConf.file
        if os.path.exists(self.file):
            logging.error('Socket file "%s" already exists. Probably nxlog-admin is already running' % self.file)
            raise Exception('Socket file "%s" already exists. Probably nxlog-admin is already running' % self.file)

        self.core = core
        self.create_socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(self.file)
        self.clients = []
        self.listen(5)
        logging.info('listen ctl socket "%s"', self.file)

    def handle_accept(self):
        """
        handle accept event: new connection via socket file incoming
        """
        sock, addr = self.accept()
        logging.info('accepting connection "%s"', self.file)
        self.clients.append(Client(sock, self.file, self.core))


class Client(asyncore.dispatcher_with_send):
    """
    ctl connection client
    """

    def __init__(self, sock, addr, core):
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.file = addr
        self.core = core
        self.session = api.action.Session(addr, self)

    def handle_read(self):
        """
        has incoming data
        """
        data = self.recv(1024)
        logging.info('read data from %s: "%s"', self.file, data)
        if self.session.read(data):
            # session read all needed data
            self.core.execute(self.session)
            self.session.clear()

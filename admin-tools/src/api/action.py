import logging
import json
from api.nxerrors import Missed, Offline
from api.remote import AgentStatus
import sys
import traceback


class Action:

    def __init__(self, name, description=None):
        self.name = name
        self.description = description
        self.actions = None

    def execute(self, core, session):
        pass

    def prepare_out(self, session):
        session.out["func"] = self.name

    def get_agent(self, core, session):
        agent = core.agents.get(session.id)
        if agent.status == AgentStatus.OFFLINE:
            raise Offline(agent.ip)
        return agent


class Session:

    SUCCESS = 0

    def __init__(self, file, connection=None):
        self.clear()
        self.file = file
        self.connection = connection
        self.out = {"result": "success"}

    def read(self, data):
        self.buffer += data
        try:
            logging.debug('try to parse %s', self.buffer)
            self.json = json.loads(self.buffer)
            self.func = self.json['func']
            logging.debug('ok, json=%s', self.json)
            self.complete = True
        except:
            logging.warning('failed, wait')
            self.complete = False
        return self.complete

    def pick_error(self, num, reason):
        self.out["result"] = "error"
        self.out["errnum"] = str(num)
        self.out["reason"] = reason

    def clear(self):
        self.buffer = ''
        self.complete = False

    def add_element(self, tag='elements', **kwargs):
        if not tag in self.out:
            self.out[tag] = []
        self.out[tag].append(kwargs)

    def add_params(self, **kwargs):
        self.out = dict(self.out, **kwargs)

    def add_dict(self, param):
        for name, val in param.iteritems():
            self.out[name] = str(val)

    def __getattr__(self, name):
        logging.info('getting session.%s', name)
        if name not in self.json:
            logging.error('failed get session value %s', name)
            raise Missed("field", name)
        value = self.json[name]
        logging.debug('returns %s = %s', name, value)
        return value

#!/bin/sh

# This script is an example of how to use curl to send an 
# HTTP POST request to an im_http instance in NXLog. It
# uses HTTPS and sends plain text data passed as a command
# argument.

# Replace with the hostname and port that im_http has 
# been configured to listen on.
URL=https://<hostname>
PORT=<port>

# Replace with the path to the client certificate, client 
# certificate key, and certification authority files respectively.
USESSL="--cert keys/client-cert.pem --key keys/client-key.pem --cacert keys/ca.pem"

# Calculates the size of the first command argument $1.
BYTELEN=`printf "%s" "$1" | wc -c`

curl -v $USESSL -H "Content-Type:plain/text" -H "Content-Length:${BYTELEN}" -d "$1" $URL:$PORT
